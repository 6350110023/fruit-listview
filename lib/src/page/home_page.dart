
import 'package:flutter/material.dart';
import 'package:fruit_listview/src/models/fruit_data_model.dart';
import 'package:fruit_listview/src/page/furuit_detail.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<HomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {

  static List<String> fruitname = ['Apple','Banana','Mango','Orange','PineApple'];
  static List url = [
    'https://www.applesfromny.com/wp-content/uploads/2020/05/Jonagold_NYAS-Apples2.png',
    'https://cdn.mos.cms.futurecdn.net/42E9as7NaTaAi4A6JcuFwG-1200-80.jpg',
    'https://media.istockphoto.com/photos/mango-isolated-on-white-background-picture-id911274308?k=20&m=911274308&s=612x612&w=0&h=YY8-xqycxsqFea5B-JdhlcgExlXYWMiFoLJdQ-LUx5E=',
    'https://5.imimg.com/data5/VN/YP/MY-33296037/orange-600x600-500x500.jpg',
    'https://5.imimg.com/data5/GJ/MD/MY-35442270/fresh-pineapple-500x500.jpg'
  ];
  static List description = [
    'Apple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Banana is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Mango is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Orange is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Pineapple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
  ];

  final List<FruitDataModel> Fruitdata = List.generate(fruitname.length, (index) =>
  FruitDataModel('${fruitname[index]}','${url[index]}', '${description[index]}'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
          itemCount: Fruitdata.length,
          itemBuilder: (context,index){
            return Container(
              height: 100,
              child: Card(
                child: Align(
                  alignment: Alignment.center,
                  child: ListTile(
                    title: Text(
                      Fruitdata[index].name,
                      style: TextStyle(fontSize: 25),
                    ),
                    leading: SizedBox(
                      width: 70,
                      height: 70,
                      child: Image.network(Fruitdata[index].ImageUrl),
                    ),
                    onTap: (){
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FruitDetail(fruitDataModel: Fruitdata[index])
                        ),
                      );
                    },
                  ),
                ),
              ),
            );
    }
      ),
    drawer: Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
       children: [
         UserAccountsDrawerHeader(
             accountName: const Text('Nattapat Chaiphet'),
             accountEmail: const Text('6350110023@psu.ac.th'),
           currentAccountPicture: CircleAvatar(backgroundImage: AssetImage('assets/images/nut.jpg'),
           ),
         ),
         ListTile(
             title: const Text('Home'),
             leading: const Icon(Icons.home),
             ),
         ListTile(
             title: const Text('Apple'),
             leading: const Icon(Icons.apple),
           onTap: (){
             Navigator.of(context).push(
               MaterialPageRoute(
                   builder: (context) => FruitDetail(fruitDataModel: Fruitdata[0])
               ),
             );
           },
             ),
         ListTile(
             title: const Text('Banana'),
             leading: const Icon(Icons.emoji_food_beverage),
             onTap: (){
             Navigator.of(context).push(
               MaterialPageRoute(
                   builder: (context) => FruitDetail(fruitDataModel: Fruitdata[1])
               ),
             );
           },
             ),
         ListTile(
             title: const Text('Mango'),
             leading: const Icon(Icons.fiber_manual_record),
           onTap: (){
             Navigator.of(context).push(
               MaterialPageRoute(
                   builder: (context) => FruitDetail(fruitDataModel: Fruitdata[2])
               ),
             );
           },
         ),
         ListTile(
             title: const Text('Orange'),
             leading: const Icon(Icons.online_prediction),
             onTap: (){
             Navigator.of(context).push(
               MaterialPageRoute(
                   builder: (context) => FruitDetail(fruitDataModel: Fruitdata[3])
               ),
             );
           },
         ),
         ListTile(
             title: const Text('Pineapple'),
             leading: const Icon(Icons.pin_end),
             onTap: (){
             Navigator.of(context).push(
               MaterialPageRoute(
                   builder: (context) => FruitDetail(fruitDataModel: Fruitdata[4])
               ),
             );
           },
             ),


       ],
      )
    ),
    );
  }
}
